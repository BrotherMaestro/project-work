﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EthicGame
{
    public interface IDeckMechanic
    {
        bool HasCards();
        void LoadCard(Card c);
        Card DrawCard();
        void ClearDeck();
    }
}