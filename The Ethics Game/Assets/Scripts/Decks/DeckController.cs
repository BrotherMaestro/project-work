﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace EthicGame
{
    public class DeckController : IDeckMechanic
    {
        StandardDeck techDeck;
        StandardDeck companyDeck;
        StandardDeck worldDeck;
        GreyDeck greyDeck;
        EndDeck endDeck;

        List<DrawProbabilityBuff> buffs;
        List<DrawProbabilityBuff> buffsToRemove;
        System.Random random;
        double techProb;
        double compProb;
        double worlProb;
        double greyProb;

        public DeckController()
        {
            techDeck = new StandardDeck();
            companyDeck = new StandardDeck();
            worldDeck = new StandardDeck();
            greyDeck = new GreyDeck();
            endDeck = new EndDeck();

            buffs = new List<DrawProbabilityBuff>();
            buffsToRemove = new List<DrawProbabilityBuff>();
            random = new System.Random();

            techProb = 34.0d;
            compProb = 33.0d;
            worlProb = 33.0d;
        }

        public bool HasCards()
        {
            if (techDeck.HasCards() || companyDeck.HasCards() || worldDeck.HasCards() || greyDeck.HasCards() )
             return true; 
            else
             return false; 
        }


        public void LoadCard(Card c)
        {
            if (c == null) return;
            if (c.storyType is TechType)
            {
                techDeck.LoadCard(c);
            }
            else if (c.storyType is CompanyType)
            {
                companyDeck.LoadCard(c);
            }
            else if (c.storyType is WorldType)
            {
                worldDeck.LoadCard(c);
            }
            else if (c.storyType is GreyType)
            {
                greyDeck.LoadCard(c);
            }
            else if (c.storyType is EndCard)
            {
                endDeck.LoadCard(c);
            }
        }

        
        public Card DrawCard()
        {
            //Check for empty deck
            Card result = null;
            if (!HasCards()) return result;

            UpdateProbabilities();
            Debug.Log("Deck Probs: " + techProb + " " + compProb + " " + worlProb + " " + greyProb);

            //Must have cards so rollup with probs, rolldown as backup
            double rollDice = random.NextDouble() * 100d;
            if (rollDice < techProb && techDeck.HasCards() ) result = techDeck.DrawCard();
            else if (rollDice < techProb + compProb && companyDeck.HasCards() ) result = companyDeck.DrawCard();
            else if (worldDeck.HasCards()) result = worldDeck.DrawCard();
            else if (companyDeck.HasCards() ) result = companyDeck.DrawCard();
            else if (techDeck.HasCards()) result = techDeck.DrawCard();
            else result = greyDeck.DrawCard();

            return result;
        }


        public Card DrawGrey()
        {
            return greyDeck.HasCards()? greyDeck.DrawCard() : null;
        }

        // Initialise a new sequence of cards and draws the first card (first card can be null)
        public Card LoadLevel(LevelDeckPreset levelPreset)
        {
            List<Card> presetCards = levelPreset.presetCards;
            for (int i = 0; i < presetCards.Count; i++)
            {
                LoadCard(presetCards[i]);
            }

            return levelPreset.startCard;
        }


        public void SetEndDeckProbs(bool pres, bool tox, bool happ, bool stab, bool posEnd, bool negEnd)
        {
            endDeck.preservation = pres;
            endDeck.toxicity = tox;
            endDeck.happiness = happ;
            endDeck.stability = stab;
            endDeck.positiveEnd = posEnd;
            endDeck.negativeEnd = negEnd;
        }

        public Card DrawEnd()
        {
            return endDeck.DrawCard();
        }


        //Hard Game Reset
        public void ClearDeck()
        {
            techDeck.ClearDeck();
            companyDeck.ClearDeck();
            worldDeck.ClearDeck();
            greyDeck.ClearDeck();
            endDeck.ClearDeck();
        }


        public void AddProbabilities(List<Buff> newBuffs)
        {
            foreach(Buff buff in newBuffs)
            {
                if (buff is DrawProbabilityBuff adjustedBuff)
                {
                    buffs.Add(adjustedBuff);

                    //Perform immediate update
                    ProbabilityShift(adjustedBuff);
                }
            }
        }

        // Check for the end of a Buffs lifetime
        void UpdateProbabilities()
        {
            foreach (DrawProbabilityBuff buff in buffs)
            {
                if (buff.delay > 0)
                {
                    buff.delay -= 1;
                    continue;
                }
                else if (buff.duration == 0)
                {
                    ProbabilityDeShift(buff);
                    buffsToRemove.Add(buff);
                }
                else if (buff.duration > 0)
                {
                    buff.duration -= 1;
                }
            }
            for (int i = 0; i < buffsToRemove.Count; i++)
            {
                buffs.Remove(buffsToRemove[i]);
            }
            buffsToRemove.Clear();

            if (buffs.Count == 0) ResetProbababilities();

        }

        // Functions to reassess and apply new probabilities to the deck
        void ProbabilityShift(DrawProbabilityBuff buff)
        {
            double shiftProb = buff.GetShiftValue();
            ProbabilityCalc(shiftProb, buff);
        }

        void ProbabilityDeShift(DrawProbabilityBuff buff)
        {
            double shiftProb = -buff.GetShiftValue();
            ProbabilityCalc(shiftProb, buff);
        }

        void ResetProbababilities()
        {
            techProb = 33.0d;
            compProb = 33.0d;
            worlProb = 33.0d;
            greyProb = 1.0d;
        }

        void ProbabilityCalc(double shift, DrawProbabilityBuff buff)
        {
            CardType buffType = buff.GetDeckType();
            if (buffType is TechType)
            {
                double offset = offsetCalc(shift, techProb);
                techProb += offset;
                ProbDivider(offset);
            }
            else if (buffType is CompanyType)
            {
                double offset = offsetCalc(shift, compProb);
                compProb += offset;
                ProbDivider(offset);
            }
            else if (buffType is WorldType)
            {
                double offset = offsetCalc(shift, worlProb);
                worlProb += offset;
                ProbDivider(offset);
            }
            else if (buffType is GreyType)
            {
                double offset = offsetCalc(shift, greyProb);
                greyProb += offset;
                ProbDivider(offset);
            }
        }

        double offsetCalc(double shiftProb, double currProb)
        {
            double newProb = shiftProb + currProb;
            return 100d * (shiftProb) / (100d - newProb);
        }

        void ProbDivider(double offset)
        {
            techProb *= 100d / (100d + offset);
            compProb *= 100d / (100d + offset);
            worlProb *= 100d / (100d + offset);
            greyProb *= 100d / (100d + offset);
        }
      
    }
}