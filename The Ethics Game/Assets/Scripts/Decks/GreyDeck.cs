﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace EthicGame
{

    public class GreyDeck : IDeckMechanic
    {
        List<Card> CardsInDeck;
        System.Random random;

        public GreyDeck()
        {
            CardsInDeck = new List<Card>();
            random = new System.Random();
        }

        public bool HasCards()
        {
            return (CardsInDeck.Count > 0) ? true : false;
        }

        public void LoadCard(Card c)
        {
            CardsInDeck.Add(c);
        }

        public Card DrawCard()
        {
            int cardCount = CardsInDeck.Count;
            int indexOfFate = random.Next(0, cardCount);
            Card result = CardsInDeck[indexOfFate];
            CardsInDeck.Remove(result);

            return result;
        }

        public void ClearDeck()
        {
            CardsInDeck.Clear();
        }
    }
}