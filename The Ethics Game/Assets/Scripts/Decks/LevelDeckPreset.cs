﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace EthicGame
{
    [CreateAssetMenu(menuName ="Presets/DeckPreset")]
    public class LevelDeckPreset : ScriptableObject
    {
        public Card startCard;
        public List<Card> presetCards = new List<Card>();

        public void LoadCard(Card c)
        {
            if (c == null) return;
            presetCards.Add(c);
        }

    }
}