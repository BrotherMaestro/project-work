﻿using UnityEngine;
using System.Collections;


namespace EthicGame
{
    public class EndDeck : IDeckMechanic
    {
        GreyDeck PreservationEnd;
        GreyDeck ToxicityEnd;
        GreyDeck HappyEnd;
        GreyDeck UnstableEnd;
        GreyDeck MediocreEnd;
        GreyDeck SuperSecretDinoDeck;

        System.Random random;

        public bool preservation = false;
        public bool toxicity = false;
        public bool happiness = false;
        public bool stability = false;
        public bool positiveEnd = false;
        public bool negativeEnd = false;

        public EndDeck()
        {
            PreservationEnd = new GreyDeck();
            ToxicityEnd = new GreyDeck();
            HappyEnd = new GreyDeck();
            UnstableEnd = new GreyDeck();
            MediocreEnd = new GreyDeck();
            SuperSecretDinoDeck = new GreyDeck();
            random = new System.Random();
        }

        public void ClearDeck()
        {
            PreservationEnd.ClearDeck();
            ToxicityEnd.ClearDeck();
            HappyEnd.ClearDeck();
            UnstableEnd.ClearDeck();
            MediocreEnd.ClearDeck();

            preservation = false;
            toxicity = false;
            happiness = false;
            stability = false;
            positiveEnd = false;
            negativeEnd = false;
        }

        public Card DrawCard()
        {
            if (SuperSecretDinoDeck.HasCards()) return SuperSecretDinoDeck.DrawCard();

            else if (preservation && PreservationEnd.HasCards()) return PreservationEnd.DrawCard();
            else if (toxicity && ToxicityEnd.HasCards()) return ToxicityEnd.DrawCard();
            else if (happiness && HappyEnd.HasCards()) return HappyEnd.DrawCard();
            else if (stability && UnstableEnd.HasCards()) return UnstableEnd.DrawCard();
            else if (positiveEnd && (PreservationEnd.HasCards() || HappyEnd.HasCards())) return DrawPositive();
            else if (negativeEnd && (ToxicityEnd.HasCards() || UnstableEnd.HasCards())) return DrawNegative();
            else
            {
                return MediocreEnd.DrawCard();
            }
        }

        public bool HasCards()
        {
            if (PreservationEnd.HasCards() || HappyEnd.HasCards() ||
                ToxicityEnd.HasCards() || UnstableEnd.HasCards() ||
                MediocreEnd.HasCards())
            {
                return true;
            }
            else return false;
        }

        public void LoadCard(Card c)
        {
            if (c.storyType is PreservationEnd) PreservationEnd.LoadCard(c);
            else if (c.storyType is ToxicityEnd) ToxicityEnd.LoadCard(c);
            else if (c.storyType is HappinessEnd) HappyEnd.LoadCard(c);
            else if (c.storyType is StabilityEnd) UnstableEnd.LoadCard(c);
            else if (c.storyType is MediocreEnd) MediocreEnd.LoadCard(c);
            else if (c.storyType is SuperSecretDinoEnding) SuperSecretDinoDeck.LoadCard(c); 
        }


        // Prereq: Only called when one of the positive decks have a card
        Card DrawPositive()
        {
            int indexOfFate = random.Next(0, 2);
            if (indexOfFate == 0 && PreservationEnd.HasCards()) return PreservationEnd.DrawCard();
            else if (indexOfFate == 1 && HappyEnd.HasCards()) return HappyEnd.DrawCard();
            else if (PreservationEnd.HasCards()) return PreservationEnd.DrawCard();
            else return HappyEnd.DrawCard();
        }

        Card DrawNegative()
        {
            int indexOfFate = random.Next(0, 2);
            if (indexOfFate == 0 && ToxicityEnd.HasCards()) return ToxicityEnd.DrawCard();
            else if (indexOfFate == 1 && UnstableEnd.HasCards()) return UnstableEnd.DrawCard();
            else if (ToxicityEnd.HasCards()) return ToxicityEnd.DrawCard();
            else return UnstableEnd.DrawCard();
        }

    }


}