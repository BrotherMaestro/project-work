﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace EthicGame
{
    public class StandardDeck : IDeckMechanic
    {
        struct TicketSlave
        {
            public int tickets;
            readonly Card c;

            public TicketSlave(int ticketInit, Card card)
            {
                tickets = ticketInit;
                c = card;
            }

            public Card GetCard()
            {
                return c;
            }

            public void IncTickets()
            {
                tickets += 1;
            }
        }

        List<TicketSlave> CardsInDeck;
        int totalTickets;
        int ticketLimit;
        System.Random random;


        public StandardDeck()
        {
            totalTickets = 0;
            ticketLimit = 3;
            CardsInDeck = new List<TicketSlave>();
            random = new System.Random();
        }

        public bool HasCards()
        {
            return (CardsInDeck.Count > 0) ? true : false; 
        }


        public void LoadCard(Card c)
        {
            TicketSlave cardSlot = new TicketSlave(1, c);
            CardsInDeck.Add(cardSlot);
        }


        // Precondition: TotalTickets is equal to the total number of ticket entries in every struct
        public Card DrawCard()
        {
            Card result = null;            
            if (CardsInDeck.Count > 0)
            {
                TicketSlave currentSlot = CardsInDeck[0];
                int rollDice = random.Next(0, totalTickets);
                for (int i = 0; i < CardsInDeck.Count; i++)
                {
                    currentSlot = CardsInDeck[i];
                    rollDice -= currentSlot.tickets;
                    if (rollDice < 0)
                    {
                        break;
                    }
                }
                // Grab the winning card and remove from deck
                result = currentSlot.GetCard();
                CardsInDeck.Remove(currentSlot);
            }
            //Update Total Tickets and increment individual tickets
            return result;
        }


        public void ClearDeck()
        {
            CardsInDeck.Clear();
        }


        //Recount totaltickets and update existing ticket holders
        public void TicketUpdate()
        {
            int ticketCount = 0;
            foreach (TicketSlave cardSlot in CardsInDeck)
            {
                if (cardSlot.tickets < ticketLimit)
                {
                    cardSlot.IncTickets();
                }
                ticketCount += cardSlot.tickets;
            }
            totalTickets = ticketCount;
        }

    }
}