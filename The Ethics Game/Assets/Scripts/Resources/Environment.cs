﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


namespace EthicGame
{
    public class Environment : IResource<int>
    {
        // Environment Values 
        int preservation;
        int toxicity;

        // Environment Buff Variables
        List<Buff> baseModifiers;
        List<Buff> auraModifiers;
        List<Buff> toRemove;

        public Environment()
        {
            //Curretn world is not perfect, but a lot more can go wrong
            preservation = 85;
            toxicity = -15;

            baseModifiers = new List<Buff>();
            auraModifiers = new List<Buff>();
            toRemove = new List<Buff>();
        }

        public void AddBuff(Buff buff)
        {
            if (!(buff is IResourceBuff<int>)) return;
            if (buff is Sum)
            {
                baseModifiers.Add(buff);
            }
            else if (buff is Aura)
            {
                auraModifiers.Add(buff);
            }
        }

        public void ClearBuffs()
        {
            baseModifiers.Clear();
            auraModifiers.Clear();
        }

        public int GetValue()
        {
            return preservation;
        }

        public int GetCounterValue()
        {
            return toxicity;
        }

        public void NextLevel()
        {
            // nothing as of yet
        }

        public void Reset()
        {
            preservation = 85;
            toxicity = -15;
            ClearBuffs();
        }

        public void StepResources()
        {
            foreach (Buff buff in baseModifiers)
            {
                if (buff.buffTarget is EnvPosBuff) ModifyPreservation(buff);
                else if (buff.buffTarget is EnvNegBuff) ModifyToxicity(buff);
            }
            RemovalAdmin(baseModifiers);

            // Cap Values
            //Debug.Log("Env / Tox: " + preservation + " " + toxicity);
            if (preservation > 100) preservation = 100;
            else if (preservation < 0) preservation = 0;
            if (toxicity > 0) toxicity = 0;
            else if (toxicity < -100) toxicity = -100;
        }

        public void StepInfluence()
        {
            ModifyAura();
        }

        void ModifyPreservation(Buff buff)
        {
            IResourceBuff<int> mod = (IResourceBuff<int>)buff;
            int input = 0;
            bool inactive = mod.Modify(ref input);
            if (inactive) toRemove.Add(buff);
            else preservation += input;
        }

        void ModifyToxicity(Buff buff)
        {
            IResourceBuff<int> mod = (IResourceBuff<int>)buff;
            int input = 0;
            bool inactive = mod.Modify(ref input);
            if (inactive) toRemove.Add(buff);
            else toxicity += input;
        }

        void ModifyAura()
        {
            foreach (Buff buff in auraModifiers)
            {
                Aura aura = (Aura)buff;
                IResourceBuff<int> mod = (IResourceBuff<int>)buff;
                int input;
                if (aura.buffTarget is EnvPosBuff) input = preservation;
                else input = toxicity;
                bool inactive = mod.Modify(ref input);
                if (inactive)
                {
                    toRemove.Add(buff);
                }
            }
            RemovalAdmin(auraModifiers);
        }

        void RemovalAdmin(List<Buff> buffList)
        {
            foreach (Buff oldBuff in toRemove)
            {
                buffList.Remove(oldBuff);
            }
            toRemove.Clear();
        }
    }
}