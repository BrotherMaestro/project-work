﻿using UnityEngine;
using System.Collections;

namespace EthicGame
{
    public interface IResource<T>
    {
        T GetValue();
        void StepResources();
        void StepInfluence();
        void NextLevel();
        void Reset();
        void AddBuff(Buff buff);
        void ClearBuffs();
    }
}