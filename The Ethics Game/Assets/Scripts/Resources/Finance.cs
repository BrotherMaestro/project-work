﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace EthicGame
{
    public class Finance : IResource<long>
    {
        // Finance Bank Value Variables
        long bank;
        long[] baseIncome = { 10000, 250000, 10000000 };
        long[] bankCapacity = { 500000, 10000000, 1000000000 };
        int level;

        // Finance Buff Variables
        List<Buff> baseModifiers;
        List<Buff> totalModifiers;
        List<Buff> auraModifiers;
        List<Buff> scaledModifiers;
        List<Buff> toRemove;

        public Finance()
        {
            bank = 30000;
            level = 0;
            baseModifiers = new List<Buff>();
            totalModifiers = new List<Buff>();
            auraModifiers = new List<Buff>();
            scaledModifiers = new List<Buff>();
            toRemove = new List<Buff>();
        }

        /*
         * Precondition: All Finanacial type buffs sent here are the long version 
         * */
        public void AddBuff(Buff buff)
        {
            if (!(buff is IResourceBuff<long>)) return;
            if ( buff is Multiplier)
            {
                totalModifiers.Add(buff);
            }
            else if ( buff is Probability || buff is Sum)
            {
                baseModifiers.Add(buff);
            }
            else if (buff is Aura)
            {
                auraModifiers.Add(buff);
            }
            else if (buff is ScaledProbability)
            {
                scaledModifiers.Add(buff);
            }
        }

        public void ClearBuffs()
        {
            baseModifiers.Clear();
            totalModifiers.Clear();
            auraModifiers.Clear();
            scaledModifiers.Clear();
        }

        public long GetValue()
        {
            return bank;
        }

        public long GetCapacity()
        {
            return bankCapacity[level];
        }

        public void NextLevel()
        {
            if(level < 2) level += 1;

            switch(level)
            {
                // Level 2
                case 1:
                    bank = 550000;
                    break;
                case 2:
                    bank = 15000000;
                    break;
            }
        }

        public void Reset()
        {
            bank = 30000;
            level = 0;
            ClearBuffs();
        }

        public void StepResources()
        {
            //Calculate the income recieved this turn
            long baseStepA = ModifyBase();
            long baseStepB = ModifyTotal();
            long baseStepC = ModifyScaled();

            bank += baseStepA + baseStepB + baseStepC;
            //Clamp the result
            if (bank >= bankCapacity[level]) bank = bankCapacity[level];
            else if (bank < 0) bank = 0;
        }

        public void StepInfluence()
        {
            //Calculate potential influence on other resources
            ModifyAura();
        }


        long ModifyBase()
        {
            List<long> modVals = new List<long>();

            foreach(Buff buff in baseModifiers)
            {
                IResourceBuff<long> mod = (IResourceBuff<long>)buff;
                long input = baseIncome[level];
                bool inactive = mod.Modify(ref input);
                if (inactive)
                {
                    toRemove.Add(buff);
                    continue;
                }
                else
                {
                    modVals.Add(input);
                }
            }
            RemovalAdmin(baseModifiers);
            return (TotalValueAdmin(modVals) + baseIncome[level]);
        }

        long ModifyTotal()
        {
            List<long> modVals = new List<long>();

            foreach (Buff buff in totalModifiers)
            {
                IResourceBuff<long> mod = (IResourceBuff<long>)buff;
                long input = bank;
                bool inactive = mod.Modify(ref input);
                if (inactive)
                {
                    toRemove.Add(buff);
                    continue;
                }
                else
                {
                    modVals.Add(input);
                }
            }
            RemovalAdmin(totalModifiers);
            return TotalValueAdmin(modVals); 
        }

        long ModifyScaled()
        {
            List<long> modVals = new List<long>();

            foreach(Buff buff in scaledModifiers)
            {
                IResourceBuff<long> mod = (IResourceBuff<long>)buff;
                long input = bankCapacity[level];
                bool inactive = mod.Modify(ref input);
                if (inactive)
                {
                    toRemove.Add(buff);
                    continue;
                }
                else
                {
                    input = input / Convert.ToInt64( Math.Pow(10d, Convert.ToDouble(level) ) ) ;
                    Debug.Log("Power division in Finance: " + input);
                    modVals.Add(input);
                }
            }
            RemovalAdmin(scaledModifiers);
            return TotalValueAdmin(modVals);
        }

        void ModifyAura()
        {
            foreach (Buff buff in auraModifiers)
            {
                IResourceBuff<long> mod = (IResourceBuff<long>)buff;
                long input = bank;
                bool inactive = mod.Modify(ref input);
                if (inactive)
                {
                    toRemove.Add(buff);
                }
            }
            RemovalAdmin(auraModifiers);
        }

        long TotalValueAdmin(List<long> modVals)
        {
            long step = 0;
            foreach (long val in modVals)
            {
                step += val;
            }
            return step;
        }

        void RemovalAdmin(List<Buff> buffList)
        {
            foreach (Buff oldBuff in toRemove)
            {
                buffList.Remove(oldBuff);
            }
            toRemove.Clear();
        }

    }
}