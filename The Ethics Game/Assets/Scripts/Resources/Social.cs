﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;




namespace EthicGame
{
    public class Social : IResource<int>
    {
        // Public Happiness and Social Stability
        int PH;
        int SS;

        // Social Buff Variables
        List<Buff> baseModifiers;
        List<Buff> auraModifiers;
        List<Buff> toRemove;

        public Social()
        {
            PH = 60;
            SS = -5;

            baseModifiers = new List<Buff>();
            auraModifiers = new List<Buff>();
            toRemove = new List<Buff>();
        }

        public void AddBuff(Buff buff)
        {
            if (!(buff is IResourceBuff<int>)) return;
            if (buff is Sum)
            {
                baseModifiers.Add(buff);
            }
            else if (buff is Aura)
            {
                auraModifiers.Add(buff);
            }
        }

        public void ClearBuffs()
        {
            baseModifiers.Clear();
            auraModifiers.Clear();
        }

        public void Reset()
        {
            PH = 60;
            SS = -5;
            ClearBuffs();
        }

        public int GetValue()
        {
            return PH;
        }

        public int GetCounterValue()
        {
            return SS;
        }

        public void NextLevel()
        {
            // Nothing as of yet
        }

        public void StepResources()
        {
            foreach(Buff buff in baseModifiers)
            {
                if (buff.buffTarget is SocPosBuff) ModifyPH(buff);
                else if (buff.buffTarget is SocNegBuff) ModifySS(buff);
            }
            RemovalAdmin(baseModifiers);

            //Cap the values
            //Debug.Log("PH / SS: " + PH + " " + SS);
            if (PH > 100) PH = 100;
            else if (PH < 0) PH = 0;
            if (SS > 0) SS = 0;
            else if (SS < -100) SS = -100;
        }

        public void StepInfluence()
        {
            //Calculate potential influence on other resources
            ModifyAura();
        }

        void ModifyPH(Buff buff)
        {
            IResourceBuff<int> mod = (IResourceBuff<int>)buff;
            int input = 0;
            bool inactive = mod.Modify(ref input);
            if (inactive) toRemove.Add(buff);
            else PH += input;
        }

        void ModifySS(Buff buff)
        {
            IResourceBuff<int> mod = (IResourceBuff<int>)buff;
            int input = 0;
            bool inactive = mod.Modify(ref input);
            if (inactive) toRemove.Add(buff);
            else SS += input;
        }

        void ModifyAura()
        {
            foreach (Buff buff in auraModifiers)
            {
                Aura aura = (Aura)buff;
                IResourceBuff<int> mod = (IResourceBuff<int>)buff;
                int input;
                if (aura.buffTarget is SocPosBuff) input = PH;
                else input = SS;
                bool inactive = mod.Modify(ref input);
                if (inactive)
                {
                    toRemove.Add(buff);
                }
            }
            RemovalAdmin(auraModifiers);
        }

        void RemovalAdmin(List<Buff> buffList)
        {
            foreach (Buff oldBuff in toRemove)
            {
                buffList.Remove(oldBuff);
            }
            toRemove.Clear();
        }
    }
}