﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace EthicGame
{
    public class Manpower : IResource<int>
    {
        // Manpower Value Variables
        int manpwr;
        int[] baseIncome = { 5, 4, 2 };
        int level;

        // Manpower Buff Variables
        List<Buff> baseModifiers;
        List<Buff> scaledModifiers;
        List<Buff> auraModifiers;
        List<Buff> toRemove;

        public Manpower()
        {
            manpwr = 30;
            level = 0;

            baseModifiers = new List<Buff>();
            scaledModifiers = new List<Buff>();
            auraModifiers = new List<Buff>();
            toRemove = new List<Buff>();
        }

        public void AddBuff(Buff buff)
        {
            if (!(buff is IResourceBuff<int>)) return;
            if (buff is Sum)
            {
                baseModifiers.Add(buff);
            }
            else if (buff is ScaledProbability)
            {
                scaledModifiers.Add(buff);
            }
            else if (buff is Aura)
            {
                auraModifiers.Add(buff);
            }
        }

        public void ClearBuffs()
        {
            baseModifiers.Clear();
            scaledModifiers.Clear();
            auraModifiers.Clear();
        }

        public void Reset()
        {
            manpwr = 30;
            level = 0;
            ClearBuffs();
        }

        public int GetValue()
        {
            return manpwr;
        }

        public void NextLevel()
        {
            if (level < 2) level += 1;
            manpwr = 20;
        }

        public void StepResources()
        {
            int baseStepA = ModifyBase();
            int baseStepB = ModifyScaled();

            manpwr += baseStepA + baseStepB;
            if (manpwr > 100) manpwr = 100;
            else if (manpwr < 0) manpwr = 0;
        }

        public void StepInfluence()
        {
            //Calculate potential influence on other resources
            ModifyAura();
        }

        int ModifyBase()
        {
            List<int> modVals = new List<int>();

            foreach (Buff buff in baseModifiers)
            {
                IResourceBuff<int> mod = (IResourceBuff<int>)buff;
                int input = baseIncome[level];
                bool inactive = mod.Modify(ref input);
                if (inactive)
                {
                    toRemove.Add(buff);
                    continue;
                }
                else
                {
                    modVals.Add(input);
                }
            }
            RemovalAdmin(baseModifiers);
            return (TotalValueAdmin(modVals) + baseIncome[level]);
        }

        int ModifyScaled()
        {
            List<int> modVals = new List<int>();

            foreach (Buff buff in scaledModifiers)
            {
                IResourceBuff<int> mod = (IResourceBuff<int>)buff;
                int input = 100;
                bool inactive = mod.Modify(ref input);
                if (inactive)
                {
                    toRemove.Add(buff);
                    continue;
                }
                else
                {
                    modVals.Add(input);
                }
            }
            RemovalAdmin(scaledModifiers);
            return TotalValueAdmin(modVals);
        }

        void ModifyAura()
        {
            foreach (Buff buff in auraModifiers)
            {
                IResourceBuff<int> mod = (IResourceBuff<int>)buff;
                int input = manpwr;
                bool inactive = mod.Modify(ref input);
                if (inactive)
                {
                    toRemove.Add(buff);
                }
            }
            RemovalAdmin(auraModifiers);
        }

        int TotalValueAdmin(List<int> modVals)
        {
            int step = 0;
            foreach (int val in modVals)
            {
                step += val;
            }
            return step;
        }

        void RemovalAdmin(List<Buff> buffList)
        {
            foreach (Buff oldBuff in toRemove)
            {
                buffList.Remove(oldBuff);
            }
            toRemove.Clear();
        }
    }
}