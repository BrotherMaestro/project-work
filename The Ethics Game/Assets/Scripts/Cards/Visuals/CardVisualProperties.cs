﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace EthicGame
{
    [System.Serializable]
    public class CardVisualProperties
    {
        public Text text;
        public Image img;
        public Button button;
        public CardElement element;
    }
}