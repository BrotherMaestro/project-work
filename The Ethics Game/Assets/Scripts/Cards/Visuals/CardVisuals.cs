﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

namespace EthicGame
{

    public class CardVisuals : MonoBehaviour
    {
        public Card card;
        public CardVisualProperties[] properties;
        public string companyName;

        // initialization
        void Start()
        {
            LoadCard(card);
        }


        public void LoadCard(Card c)
        {
            if (c == null)
                return;

            card = c;
            c.DeactivateKey();

            //Load the default visuals 
            for (int i = 0; i < card.properties.Length; i++)
            {
                CardProperties cp = card.properties[i];
                CardVisualProperties vp = GetProperty(cp.element);

                if (vp == null)
                    continue;
                else if (vp.element is CardElementText)
                {
                    vp.text.text = cp.stringValue;
                }
                else if (vp.element is CardElementInt)
                {
                    vp.text.text = cp.intValue.ToString();
                }
                else if (vp.element is CardElementImage)
                {
                    vp.img.sprite = cp.sprite;
                }
                else if (vp.element is CardElementButtonA)
                {
                    string key = cp.stringValue;
                    if (!card.solutions.ContainsKey(key))
                    {
                        continue;
                    }
                    string solnText = card.solutions[key]?.solutionText;
                    vp.button.gameObject.SetActive(true);
                    Text name = vp.button.GetComponentInChildren<Text>();
                    name.text = key;

                    //Now establish button action
                    string original = vp.text.text;
                    bool toggle = false;
                    vp.button.onClick.RemoveAllListeners();
                    vp.button.onClick.AddListener(() => SolutionToggled(vp.text, key, vp.button, original, ref toggle) );
                }
                                
            }

           

        }


        public CardVisualProperties GetProperty(CardElement e)
        {
            CardVisualProperties result = null;

            for (int i = 0; i < properties.Length; i++)
            {
               //Find the exact match
               if (properties[i].element == e )
                {
                    result = properties[i];
                    break;
                }
            }
            return result;
        }

        public void DeactivateButtons()
        {
            for (int i = 0; i < properties.Length; i++)
            {
                if (properties[i]?.element is CardElementButtonA)
                {
                    properties[i].button.gameObject.SetActive(false);
                }
            }
        }

        // Solution targetText, key and Clicks count
        // Prereq is that key exists
        void SolutionToggled(Text body, string key, Button button, string original, ref bool toggle)
        {
            //Swap active within inactive
            if ( toggle )
            {
                if ( body.text == card.solutions[key]?.solutionText )
                {
                    toggle = false;
                    card.DeactivateKey();
                    body.text = InjectCompanyName(original);
                    EventSystem.current.SetSelectedGameObject(null);
                }
                else
                {
                    card.SetActiveSoln(key);
                    body.text = InjectCompanyName(card.solutions[key]?.solutionText);
                    button.OnSelect(null);
                }                
            }
            else
            {
                toggle = true;
                card.SetActiveSoln(key);
                body.text = InjectCompanyName(card.solutions[key]?.solutionText);
                button.OnSelect(null); 
            }
        }


        string InjectCompanyName(string original)
        {
            string replacement = "the company";
            if (companyName != null) replacement = companyName;

            return original.Replace("%CompanyName%", replacement);
        }


    }
}