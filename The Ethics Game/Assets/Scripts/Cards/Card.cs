﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace EthicGame
{
    [CreateAssetMenu(menuName ="Cards/Card")]
    public class Card : ScriptableObject, ISerializationCallbackReceiver
    {
        public CardType storyType;
        public CardProperties[] properties;

        // Dictionary Values == Solution data
        public List<string> _keys = new List<string>();
        public List<SolutionBundle> _values = new List<SolutionBundle>();
        public Dictionary<string, SolutionBundle> solutions = new Dictionary<string, SolutionBundle>();

        // Active Solution
        private string activeKey = null;

        public void SetActiveSoln(string key)
        {
            activeKey = key;
        }
        public void DeactivateKey()
        {
            activeKey = null;
        }
        public bool HasActiveKey()
        {
            return (activeKey != null) ? true : false;
        }
        public string GetActiveKey()
        {
            return activeKey;
        }

        public void OnBeforeSerialize()
        {
            if (_keys.Count < solutions.Count)
            {
                _keys.Clear();
                _values.Clear();

                foreach (var kvp in solutions)
                {
                    _keys.Add(kvp.Key);
                    _values.Add(kvp.Value);
                }
            }
        }

        public void OnAfterDeserialize()
        {
            solutions = new Dictionary<string, SolutionBundle>();
            for (int i = 0; i < _keys.Count; i++)
            {
                if ( !solutions.ContainsKey(_keys[i]) )
                    solutions.Add(_keys[i], _values[i] ?? null);
            }
                
        }

        void OnGUI()
        {
            foreach (var kvp in solutions)
                GUILayout.Label("Key: " + kvp.Key + " value: " + kvp.Value);
        }

    }
}