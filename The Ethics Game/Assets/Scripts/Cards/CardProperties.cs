﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


namespace EthicGame
{
    [System.Serializable]
    public class CardProperties
    {
        public string stringValue;
        public int intValue;
        public Sprite sprite;
        public CardElement element;
    }
}