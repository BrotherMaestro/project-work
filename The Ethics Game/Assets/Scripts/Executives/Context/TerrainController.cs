﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Tilemaps;
using System;

namespace EthicGame
{
    public class TerrainController : ScriptableObject
    {
        // Card Effect Variables
        List<TerrainBuff> terrainBuffs;
        List<TerrainBuff> toRemove;

        // Resource Effect Variables
        public TerrainBuff[] preservationStates; // MAX SIZE 6
        public TerrainBuffList[] toxicityLandStates; // MAX SIZE 7
        public TerrainBuff[] toxicityWaterStates; // MAX SIZE 3
        public List<TerrainBuff> recreations; 
        public int oldEnv;
        public int oldTox;
        public int oldPH;

        // Based on public stability
        public TerrainBuff[] greyPalette;
        public TerrainBuff[] brightPalette;
        public int oldSS;


        public TerrainController()
        {
            terrainBuffs = new List<TerrainBuff>();
            toRemove = new List<TerrainBuff>();

        }

        public void AddTerrainBuff(TerrainBuff newBuff)
        {
                if (newBuff.obsolete) PreventMatch(newBuff);
                else terrainBuffs.Add(newBuff);      
        }

        public void StepCardEffects()
        {
            foreach(TerrainBuff terrainBuff in terrainBuffs)
            {
                if (terrainBuff.delay > 0) terrainBuff.delay -= 1;
                else
                {
                    ImposeChange(terrainBuff);
                    toRemove.Add(terrainBuff);
                }
            }
            AdminRemoval();

        }

        void ImposeChange(TerrainBuff terrainBuff)
        {
            foreach(Coord coord in terrainBuff?.coords)
            {
                Vector3Int pos = new Vector3Int(coord.x, coord.y, 0);
                Tilemap tilemap = terrainBuff.Tilemap;
                Tile tile = terrainBuff?.tile;
                tilemap.SetTile(pos, tile);
            }
        }

        void RequestChange(TerrainBuff terrainBuff)
        {
            foreach (Coord coord in terrainBuff.coords)
            {
                Vector3Int pos = new Vector3Int(coord.x, coord.y, 0);
                Tilemap tilemap = terrainBuff.Tilemap;
                Tile tile = terrainBuff.tile;
                if( !tilemap.HasTile(pos) ) tilemap.SetTile(pos, tile);
            }
        }


        //Assumes Coordinate Pairs in list
        void ImposeBox(TerrainBuff terrainBuff)
        {
            int count = 1;
            Coord curr, prev = null;
            foreach(Coord coord in terrainBuff?.coords)
            {
                if( count % 2 == 0)
                {
                    curr = coord;
                    BoxSetting(prev, curr, terrainBuff);
                }
                else
                {
                    prev = coord;
                }
                count += 1;
            }
        }

        void BoxSetting(Coord start, Coord end, TerrainBuff buff)
        {
            int x, y;
            for ( x = Math.Min(start.x, end.x);  x <= Math.Max(end.x,start.x); x++)
            {
                for (y = Math.Min(start.y, end.y); y <= Math.Max(end.y, start.y); y++)
                {
                    Vector3Int pos = new Vector3Int(x, y, 0);
                    Tilemap tilemap = buff.Tilemap;
                    Tile tile = buff.tile;
                    tilemap.SetTile(pos, tile);
                }
            }
        }

        public void EnforcePreset(LvlTerrainPreset terrainPreset)
        {
            // Immediate lvl changes
            foreach(TerrainBuff terrainBuff in terrainPreset.initialSetup)
            {
                ImposeChange(terrainBuff);
            }

            // Delayed lvl changes
            foreach(TerrainBuff delayedBuff in terrainPreset.DelayedConstruction)
            {
                AddTerrainBuff(delayedBuff);
            }
        }

        public void AdjustPreservation(int preservation)
        {
            if (preservationStates == null) return;
            int indicator = CheckHysteresis(preservation, ref oldEnv);
            for (int i = 0; i < preservationStates.Length; i++)
            {
                if (i < indicator) RequestChange(preservationStates[i]);
                else CleanMap(preservationStates[i]);
            }
        }

        public void AdjustToxicity(int toxicity)
        {
            if (toxicityLandStates.Length < 7 || toxicityWaterStates.Length < 3) return;
            int indicator = CheckHysteresis(Math.Abs(toxicity), ref oldTox);
            switch (indicator)
            {
                //All Grass, water clean
                case 0:
                    foreach (TerrainBuff buff in toxicityLandStates?[0].terrainBuffs)
                    {
                        ImposeBox(buff);
                    }
                    ImposeBox(toxicityWaterStates?[0]);
                    break;
                //Mostly Grass, water clean
                case 1:
                    foreach (TerrainBuff buff in toxicityLandStates?[1].terrainBuffs)
                    {
                        ImposeBox(buff);
                    }
                    ImposeBox(toxicityWaterStates?[0]);
                    break;
                //Noticeable Dirt, water clean
                case 2:
                    foreach (TerrainBuff buff in toxicityLandStates?[2].terrainBuffs)
                    {
                        ImposeBox(buff);
                    }
                    ImposeBox(toxicityWaterStates?[0]);
                    break;
                //Spreading Dirt, water clean
                case 3:
                    foreach (TerrainBuff buff in toxicityLandStates?[3].terrainBuffs)
                    {
                        ImposeBox(buff);
                    }
                    ImposeBox(toxicityWaterStates?[0]);
                    break;
                //Spreading Dirt, water murky
                case 4:
                    foreach (TerrainBuff buff in toxicityLandStates?[4].terrainBuffs)
                    {
                        ImposeBox(buff);
                    }
                    ImposeBox(toxicityWaterStates?[1]);
                    break;

                //Dirt Plague, water murky
                case 5:
                    foreach (TerrainBuff buff in toxicityLandStates?[5].terrainBuffs)
                    {
                        ImposeBox(buff);
                    }
                    ImposeBox(toxicityWaterStates?[1]);
                    break;
                //Dirt Storm, water dead
                case 6:
                    foreach (TerrainBuff buff in toxicityLandStates?[6].terrainBuffs)
                    {
                        ImposeBox(buff);
                    }
                    ImposeBox(toxicityWaterStates?[2]);
                    break;

            }
        
        }

        

        public void AdjustRecreations(int PH)
        {
            if (recreations == null) return;
            int indicator = CheckHysteresis(PH, ref oldPH);
            int count = 0;
            foreach(TerrainBuff terrainBuff in recreations)
            {
                if (count < indicator) RequestChange(terrainBuff);
                else CleanMap(terrainBuff);
                count += 1;
            }
        }

        // Changes two buildings at a time (including shops, housing, utilities? )
        public void AdjustPalette(int SS)
        {
            if (greyPalette == null || brightPalette == null) return;
            if (greyPalette.Length < 3 || brightPalette.Length < 3) return;

            int indicator = Math.Abs(SS) - oldSS;
            if (Math.Abs(indicator) > 15)
            {
                if (indicator >= 0) ChangeToGrey();
                else ChangeToBright();
            }
            else if (SS == -100) ChangeToGrey();
            else if (SS == 0) ChangeToBright();
        }

        void CleanMapList(List<TerrainBuff> terrainBuffs)
        {
            foreach(TerrainBuff terrainBuff in terrainBuffs)
            {
                CleanMap(terrainBuff);
            }
        }

        void CleanMap(TerrainBuff terrainBuff)
        {
            foreach (Coord coord in terrainBuff.coords)
            {
                Vector3Int pos = new Vector3Int(coord.x, coord.y, 0);
                Tilemap tilemap = terrainBuff.Tilemap;
                if( tilemap.GetTile(pos)?.name == terrainBuff.tile.name ) tilemap.SetTile(pos, null);
            }
        }

        int CheckHysteresis(int curr, ref int prev)
        {
            int result;
            if(curr == 0 || curr == 100 || Math.Abs(curr - prev) > 7)
            {
                prev = curr;
                result = CheckShift(curr);
            }
            else
            {
                result = CheckShift(prev);
            }
            return result;
        }

        int CheckShift(int indicator)
        {
            if (indicator > 95) return 6;
            if (indicator > 80) return 5;
            if (indicator > 60) return 4;
            if (indicator > 40) return 3;
            if (indicator > 20) return 2;
            if (indicator > 0) return 1;
            else return 0;
        }


        void ChangeToBright()
        {
            Tilemap Objmap = greyPalette[0].Tilemap;
            int count = 0;
            foreach (var pos in Objmap.cellBounds.allPositionsWithin)
            {
                Vector3Int local = new Vector3Int(pos.x, pos.y, pos.z);
                Tile replacement;
                string existing = Objmap.GetTile(local).name;
                if (existing == greyPalette[0].name)
                {
                    replacement = brightPalette[0].tile;
                    count += 1;
                }
                else if (existing == greyPalette[1].name)
                {
                    replacement = brightPalette[1].tile;
                    count += 1;
                }
                else if (existing == greyPalette[2].name)
                {
                    replacement = brightPalette[2].tile;
                    count += 1;
                }
                if (count == 2) return;
                else continue;
            }
        }

        void ChangeToGrey()
        {
            Tilemap Objmap = greyPalette[0].Tilemap;
            int count = 0;
            foreach (var pos in Objmap.cellBounds.allPositionsWithin)
            {
                Vector3Int local = new Vector3Int(pos.x, pos.y, pos.z);
                Tile replacement;
                string existing = Objmap.GetTile(local).name;
                if (existing == brightPalette[0].name)
                {
                    replacement = greyPalette[0].tile;
                    count += 1;
                }
                else if (existing == brightPalette[1].name)
                {
                    replacement = greyPalette[1].tile;
                    count += 1;
                }
                else if (existing == brightPalette[2].name)
                {
                    replacement = greyPalette[2].tile;
                    count += 1;
                }
                if (count == 2) return;
                else continue;
            }
        }


        // Called to signal the untimely end of a project or tech (BACKTRACK SOLUTION)  
        void PreventMatch(TerrainBuff obsoleteBuff)
        {
                terrainBuffs.Remove(obsoleteBuff);
        }

        void AdminRemoval()
        {
            foreach(TerrainBuff buff in toRemove)
            {
                terrainBuffs.Remove(buff);
            }
            toRemove.Clear();
        }

    }



}