﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


namespace EthicGame
{
    public class StringCrafter : ScriptableObject
    {
        string openingBlurb;
        List<ContextItem> currentEvents;
        List<ContextItem> historicEvents;
        List<ContextItem> toRemove;


        public StringCrafter()
        {
            openingBlurb = null;
            currentEvents = new List<ContextItem>();
            historicEvents = new List<ContextItem>();
            toRemove = new List<ContextItem>();
        }

        public void SetBlurb(ContextItem intro)
        {
            if (intro.heading != null)
            {
                openingBlurb = intro.heading + "\n" + intro.content + "\n";
            }
            else openingBlurb = intro.content + "\n";
        }

        public void ClearAll()
        {
            openingBlurb = null;
            currentEvents.Clear();
            historicEvents.Clear();
        }

        public void ClearCurrent()
        {
            currentEvents.Clear();
        }

        public void AddContextItem(ContextItem contextItem)
        {
            if (contextItem.contextClass is LevelBlurb) SetBlurb(contextItem);
            if (contextItem.contextClass is Impact) currentEvents.Add(contextItem);
            else if (contextItem.contextClass is Permanance) historicEvents.Add(contextItem);
        }

        public string BuildPanelString()
        {
            string panelString = openingBlurb;
            panelString += BuildCurrents();
            panelString += BuildPermanents();
            return panelString;
        }

        string BuildCurrents()
        {
            string activeEvents = null;

            foreach(ContextItem item in currentEvents)
            {
                if (!TransientActive(item)) continue;
                if (!TransientAlive(item))
                {
                    toRemove.Add(item);
                    continue;
                }

                //Seperate into paragraphs
                if (item.heading != null) activeEvents+= item.heading + "\n";
                activeEvents += item.content + "\n\n";
            }
            RemovalAdmin(currentEvents);
            toRemove.Clear();

            return activeEvents;
        }

        string BuildPermanents()
        {
            Dictionary<string, string> actionReports = new Dictionary<string, string>();

            foreach (ContextItem item in historicEvents)
            {
                if (actionReports.ContainsKey(item.heading))
                {
                    actionReports[item.heading] += item.content + "\n";
                }
                else
                {
                    actionReports.Add(item.heading, item.content + "\n");
                }
            }
            return ConcateReports(actionReports);
        }

        string ConcateReports(Dictionary<string, string> actionReports)
        {
            string fullReport = null;

            foreach (KeyValuePair<string, string> item in actionReports)
            {
                //All content (should) end with a newline
                fullReport += item.Key + "\n" + item.Value;
            }


            return fullReport;
        }

        bool TransientActive(ContextItem item)
        {
            if (item.delay > 0) item.delay -= 1;
            else if (item.delay == 0) return true;

            return false;
        }

        bool TransientAlive(ContextItem item)
        {
            if (item.duration > 0) item.duration -= 1;
            else if (item.duration == 0) return false;

            return true;
        }

        void RemovalAdmin(List<ContextItem> listToClear)
        {
            foreach(ContextItem item in toRemove)
            {
                listToClear.Remove(item);
            }
        }

        public void PreemptiveRemoval(ContextItem item)
        {
            currentEvents.Remove(item);
        }

    }
}