﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace EthicGame
{
    public class ContextController : MonoBehaviour
    {
        public GameObject contextPanel;
        public GameObject companyPane;
        public GameObject communityPane;
        public GameObject environmentPane;
        public GameObject OpenButton;
        public GameObject CloseButton;

        Text companyText;
        Text communityText;
        Text environmentText;

        StringCrafter companyCrafter;
        StringCrafter communityCrafter;
        StringCrafter environmentCrafter;

        void Awake()
        {
            //Hide the panel from Game Start
            contextPanel.SetActive(false);

            //Setup Text objs
            companyText = companyPane.GetComponentInChildren<Text>();
            communityText = communityPane.GetComponentInChildren<Text>();
            environmentText = environmentPane.GetComponentInChildren<Text>();

            //Setup String Crafters
            companyCrafter = ScriptableObject.CreateInstance("StringCrafter") as StringCrafter;
            communityCrafter = ScriptableObject.CreateInstance("StringCrafter") as StringCrafter;
            environmentCrafter = ScriptableObject.CreateInstance("StringCrafter") as StringCrafter;

            //Add listeners to Buttons
            Button open = OpenButton.GetComponentInChildren<Button>();
            Button close = CloseButton.GetComponentInChildren<Button>();

            open.onClick.AddListener(() => OpenContextPanel());
            close.onClick.AddListener(() => CloseContextPanel());

        }

        void OpenContextPanel()
        {
            contextPanel.SetActive(true);
        }

        void CloseContextPanel()
        {
            contextPanel.SetActive(false);
        }

        public void AddContextItems(ContextItem contextItem)
        {
            if (contextItem.obsolete) RemoveContext(contextItem);
            else if (contextItem.contextCategory is CompanyContext) companyCrafter.AddContextItem(contextItem);
            else if (contextItem.contextCategory is CommunityContext) communityCrafter.AddContextItem(contextItem);
            else if (contextItem.contextCategory is EnvironmentContext) environmentCrafter.AddContextItem(contextItem);
        }

        public void StepContexts()
        {
            companyText.text = companyCrafter.BuildPanelString();
            communityText.text = communityCrafter.BuildPanelString();
            environmentText.text = environmentCrafter.BuildPanelString();
        }

        public void RemoveContext(ContextItem item)
        {
            if (item.contextCategory is CompanyContext) companyCrafter.PreemptiveRemoval(item);
            else if (item.contextCategory is CommunityContext) communityCrafter.PreemptiveRemoval(item);
            else if (item.contextCategory is EnvironmentContext) environmentCrafter.PreemptiveRemoval(item);
        }

        public void ResetContext()
        {
            companyCrafter.ClearAll();
            communityCrafter.ClearAll();
            environmentCrafter.ClearAll();
        }

    }
}