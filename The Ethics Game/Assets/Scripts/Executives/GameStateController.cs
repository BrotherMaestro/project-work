﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Tilemaps;
using System;

namespace EthicGame
{
    public class GameStateController : MonoBehaviour
    {
        //Resources
        Finance banker;
        Manpower staff;
        Social society;
        Environment env;

        //Dominant States
        bool preservation = false;
        bool toxicity = false;
        bool happiness = false;
        bool stability = false;
        bool positiveEnd = false;
        bool negativeEnd = false;

        public bool Preservation { get => preservation; }
        public bool Toxicity { get => toxicity; }
        public bool Happiness { get => happiness; }
        public bool Stability { get => stability;  }
        public bool RandomPos { get => positiveEnd; }
        public bool RandomNeg { get => negativeEnd;  }


        public GameObject parentControlPanel;
        ControlPanelVisual panelVisual;

        //Context
        public GameObject contextPanel;
        ContextController contextCntr;

        //Terrain
        TerrainController terrainCntr;

        public GameObject objectMap;
        public GameObject ornamentFore;
        public GameObject ornamentAft;
        public GameObject terrainMap;

        public TerrainBuff[] preservationStates;
        public TerrainBuffList[] toxicityLandStates;
        public TerrainBuff[] toxicityWaterStates;
        public List<TerrainBuff> recreations; //Max 5 recreations
        public TerrainBuff[] greyPalette;
        public TerrainBuff[] brightPalette;

        // Company Lvl Strings
        string[] companyLvls = { "Entrepreneurial", "Dominant Business", "Innovation Monopoly" };

        void Start()
        {
            //Private Variables
            banker = new Finance();
            staff = new Manpower();
            society = new Social();
            env = new Environment();

            //Setup Unity Objects
            panelVisual = parentControlPanel.GetComponentInChildren<ControlPanelVisual>();
            contextCntr = contextPanel.GetComponentInChildren<ContextController>();
            terrainCntr = ScriptableObject.CreateInstance("TerrainController") as TerrainController;

            panelVisual.SetCompanyLvl(companyLvls[0]);

            //Get init values
            long bankVal = banker.GetValue();
            long bankCap = banker.GetCapacity();
            int manpwr = staff.GetValue();
            int PH = society.GetValue();
            int SS = society.GetCounterValue();
            int envPos = env.GetValue();
            int envNeg = env.GetCounterValue();

            //Set the init values
            panelVisual.SetBank(bankVal, bankCap);
            panelVisual.SetManpower(manpwr);
            panelVisual.SetSocialPos(PH);
            panelVisual.SetSocialNeg(SS);
            panelVisual.SetEnvPos(envPos);
            panelVisual.SetEnvNeg(envNeg);

            //Set init values of terrainCntr
            InitTerrain(PH, SS, envPos, envNeg);

        }

        void InitTerrain(int PH, int SS, int envPos, int tox)
        {
            terrainCntr.oldPH = PH;
            terrainCntr.oldSS = SS;
            terrainCntr.oldEnv = envPos;
            terrainCntr.oldTox = tox;

            for (int i = 0; i < preservationStates?.Length; i++)
            {
                preservationStates[i].Tilemap = objectMap.GetComponentInChildren<Tilemap>();
            }
            for (int i = 0; i < toxicityLandStates?.Length; i++)
            {
                foreach( TerrainBuff buff in toxicityLandStates[i].terrainBuffs )
                {
                    buff.Tilemap = terrainMap.GetComponentInChildren<Tilemap>();
                }    

            }
            for (int i = 0; i < toxicityWaterStates?.Length; i++)
            {
                toxicityWaterStates[i].Tilemap = terrainMap.GetComponentInChildren<Tilemap>();
            }


            terrainCntr.preservationStates = preservationStates;
            terrainCntr.toxicityLandStates = toxicityLandStates;
            terrainCntr.toxicityWaterStates = toxicityWaterStates;
            terrainCntr.recreations = recreations;
            terrainCntr.greyPalette = greyPalette;
            terrainCntr.brightPalette = brightPalette;

            terrainCntr.AdjustPreservation(env.GetValue());
            terrainCntr.AdjustToxicity(env.GetCounterValue());
            terrainCntr.AdjustRecreations(society.GetValue());
            terrainCntr.AdjustPalette(society.GetCounterValue());

        }

        void ResetTerrain(int PH, int SS, int envPos, int tox)
        {
            terrainCntr.oldPH = PH;
            terrainCntr.oldSS = SS;
            terrainCntr.oldEnv = envPos;
            terrainCntr.oldTox = tox;

            terrainCntr.AdjustPreservation(env.GetValue());
            terrainCntr.AdjustToxicity(env.GetCounterValue());
            terrainCntr.AdjustRecreations(society.GetValue());
            terrainCntr.AdjustPalette(society.GetCounterValue());
        }

        public bool CheckProgression()
        {
            if (banker.GetValue() == banker.GetCapacity() && staff.GetValue() == 100) return true;
            else return false;
        }

        public bool FailCondition(ref string reason)
        {
            if (banker.GetValue() == 0)
            {
                reason = "You went Bankrupt. Surprisingly, a group of staff have pooled together enough resources " +
                    "to buy the company name. Provided you sell off most of the remaining assets, " +
                    "the company will continue under a new, hopefully more proficient, ruler.";
                return true;
            }
            if (staff.GetValue() == 0)
            {
                reason = "Mutinee. A group of visionaries have moved the majority of your employee's against you." +
                    "Without any support in the office you're forced to resign. Looks like one of your trusted COO's has " +
                    "gained the trust of your senior staff and is set to step into the position. Well good luck to him. Hah!";
                return true;
            }
            else return false;
        }

        // Specifically update Finance and Manpwr without effecting the world condition
        // Doesn't account for Aura's
        public void UpdateCompanyResources()
        {
            // Find new total value for each resource
            banker.StepResources();
            long bankVal = banker.GetValue();
            long bankCap = banker.GetCapacity();

            // For now, immediately update -> might add animation inside panelVisual           
            panelVisual.SetBank(bankVal, bankCap);

            staff.StepResources();
            int manpwr = staff.GetValue();
            panelVisual.SetManpower(manpwr);
        }

        public void UpdateResources()
        {
            // Update All resource influences on other resources
            banker.StepInfluence();
            staff.StepInfluence();
            society.StepInfluence();
            env.StepInfluence();

            UpdateCompanyResources();

            society.StepResources();
            int PH = society.GetValue();
            int SS = society.GetCounterValue();
            panelVisual.SetSocialPos(PH);
            panelVisual.SetSocialNeg(SS);

            env.StepResources();
            int envPos = env.GetValue();
            int envNeg = env.GetCounterValue();
            panelVisual.SetEnvPos(envPos);
            panelVisual.SetEnvNeg(envNeg);

        }

        // Updates the Context and Terrain Information
        public void UpdateContext()
        {
            contextCntr.StepContexts();
            terrainCntr.StepCardEffects();
            terrainCntr.AdjustPreservation(env.GetValue());
            terrainCntr.AdjustToxicity(env.GetCounterValue());
            terrainCntr.AdjustRecreations(society.GetValue());
            terrainCntr.AdjustPalette(society.GetCounterValue());
        }

        //Step to next Level
        public void NextLevel()
        {
            //Progress Resources
            banker.NextLevel();
            staff.NextLevel();
            society.NextLevel();
            env.NextLevel();


            //Context panel and terrain controlled by level start card
        }

        public void ResetState()
        {
            //Reset Resources
            banker.Reset();
            staff.Reset();
            society.Reset();
            env.Reset();

            //Reset Context and Map
            contextCntr.ResetContext();
            //Clear Obj Map
            Tilemap objmap = objectMap.GetComponentInChildren<Tilemap>();
            objmap.ClearAllTiles();

            //Return to default Terrain
            ResetTerrain(society.GetValue(), society.GetCounterValue(), env.GetValue(), env.GetCounterValue());

            //Clean Misc Maps
            Tilemap foremap = ornamentFore.GetComponentInChildren<Tilemap>();
            foremap.ClearAllTiles();
            Tilemap aftmap = ornamentAft.GetComponentInChildren<Tilemap>();
            aftmap.ClearAllTiles();
        }

        // Process a chosen cards effects
        public List<Buff> AddBundleEffects(List<Buff> ResourceBuffs)
        {
            List<Buff> deckEffects = new List<Buff>();
            foreach(Buff buffSource in ResourceBuffs)
            {
                Buff buffImage = Instantiate(buffSource);
                BuffType target = buffImage.buffTarget;

                //Outlier Checks
                BuffPrep(ref buffImage);

                // Add to Deck
                if (target is DeckBuff) deckEffects.Add(buffImage);
                else if (target is FinanceBuff) banker.AddBuff(buffImage);
                else if (target is ManpowerBuff) staff.AddBuff(buffImage);
                else if (target is SocialBuff) society.AddBuff(buffImage);
                else if (target is EnvironmentBuff) env.AddBuff(buffImage);

            }
            return deckEffects;
        }

        // Addition of new content AND content to remove on match
        public void AddContextBundle(ContextBundle bundle)
        {
            if (bundle == null) return;
            foreach (ContextItem sourceItem in bundle?.ContextInfo)
            {
                contextCntr.AddContextItems(PrepareContextItem(sourceItem));
            }
            foreach(TerrainBuff sourceBuff in bundle?.terrainBuffs)
            {
                TerrainBuff imageBuff = PrepareTerrainBuff(sourceBuff);

                terrainCntr.AddTerrainBuff(imageBuff);
            }
        }

        // For edgecase management
        public void BuffPrep(ref Buff buff)
        {
            //Check for Aura
            if (buff is Aura auraBuff)
            {
                AuraPrep(ref auraBuff);
            }
        }

        // Connect Aura Buff to Resource
        public void AuraPrep(ref Aura auraBuff)
        {
            if (auraBuff is Env2Social env2SocAura) env2SocAura.SetTarget(society);
            else if (auraBuff is Env2Env env2EnvAura) env2EnvAura.SetTarget(env);
            else if (auraBuff is Social2Finance soc2BankAura) soc2BankAura.SetTarget(banker);
            else if (auraBuff is Social2Manpwr soc2ManAura) soc2ManAura.SetTarget(staff);
            else if (auraBuff is Social2Social soc2socAura) soc2socAura.SetTarget(society);
            else if (auraBuff is Social2Env soc2envAura) soc2envAura.SetTarget(env);
            else if (auraBuff is Finance2Social bankAura) bankAura.SetTarget(society);
        }

        public TerrainBuff PrepareTerrainBuff(TerrainBuff sourceBuff)
        {
            TerrainBuff imageBuff = Instantiate(sourceBuff);
            if (imageBuff.buffTarget is ObjectMap) imageBuff.Tilemap = objectMap.GetComponentInChildren<Tilemap>();
            else if (imageBuff.buffTarget is OrnamentFore) imageBuff.Tilemap = ornamentFore.GetComponentInChildren<Tilemap>();
            else if (imageBuff.buffTarget is OrnamentAft) imageBuff.Tilemap = ornamentAft.GetComponentInChildren<Tilemap>();
            else if (imageBuff.buffTarget is TerrainType) imageBuff.Tilemap = terrainMap.GetComponentInChildren<Tilemap>();
            return imageBuff;
        }

        public ContextItem PrepareContextItem(ContextItem sourceItem)
        {
            return Instantiate(sourceItem);   
        }


        // Auxillary Functions
        // Count from zero
        public void SetCompanyLevel(int level)
        {
            if (level > 2) level = 2;
            else if (level < 0) level = 0;
            panelVisual.SetCompanyLvl(companyLvls[level]);
        }

        public string ReadCompanyName()
        {
            return panelVisual.GetCompanyName();
        }


        //End Game Function
        public void SetDominantState()
        {
            int hysteresis = 10;
            int pres = Math.Abs(env.GetValue());
            int tox = Math.Abs(env.GetCounterValue());
            int happ = Math.Abs(society.GetValue());
            int stab = Math.Abs(society.GetCounterValue());

            //Check all outcomes
            if (pres >= tox + hysteresis && pres >= happ + hysteresis && pres >= stab + hysteresis) preservation = true;
            else if (tox >= pres + hysteresis && tox >= happ + hysteresis && tox >= stab + hysteresis) toxicity = true;
            else if (happ >= pres + hysteresis && happ >= tox + hysteresis && happ >= stab + hysteresis) happiness = true;
            else if (stab >= pres + hysteresis && stab >= tox + hysteresis && stab >= happ + hysteresis) stability = true;
            else if (pres + happ >= tox + stab + hysteresis) positiveEnd = true;
            else if (tox + stab >= pres + happ + hysteresis) negativeEnd = true;

        }

    }
}