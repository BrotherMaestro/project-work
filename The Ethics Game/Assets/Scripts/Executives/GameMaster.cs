﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace EthicGame
{
    public class GameMaster : MonoBehaviour
    {
        public GameObject cardPanel;
        CardVisuals cardDisplay;

        public GameObject infoPanel;
        Text infoTitle;
        Text infoBody;
        Button infoAccept;

        public GameObject controlPanel;
        public GameObject endTurnButton;
        public GameObject exitApplicationButton;
        static DeckController deckCntr;
        GameStateController stateCntr;

        public List<LevelDeckPreset> lvlCardPresets;
        int level = 1;

        //Acting as my constructor
        void Start()
        {
            // Build the Cntrs
            deckCntr = new DeckController();
            stateCntr = controlPanel.GetComponentInChildren<GameStateController>();

            //Grab Card Visual
            cardDisplay = cardPanel.GetComponentInChildren<CardVisuals>();

            //Setup the info panel
            infoTitle = infoPanel.transform.Find("Title").gameObject.GetComponentInChildren<Text>();
            infoBody = infoPanel.transform.Find("Info").gameObject.GetComponentInChildren<Text>();
            infoAccept = infoPanel.GetComponentInChildren<Button>();
            if(infoTitle == null || infoBody == null )
            {
                Debug.Log("Issue in InfoPanel setup");
            }

            if (infoAccept != null)
            {
                infoAccept.onClick.AddListener(() => CloseInfo() );
            }
            else
            {
                Debug.Log("No Button found for InfoPanel");
            }
            infoPanel.SetActive(false);

            Card first = null;
            if (lvlCardPresets.Count > 0)
            {
                //Prevent permanent changes
                List<LevelDeckPreset> tempPresets = new List<LevelDeckPreset>(); 
                foreach(LevelDeckPreset preset in lvlCardPresets)
                {
                    LevelDeckPreset replacement = Instantiate(preset);
                    tempPresets.Add(replacement);
                }
                lvlCardPresets.Clear();
                lvlCardPresets.AddRange(tempPresets);

                //Setup the first lvl
                deckCntr.LoadLevel(lvlCardPresets[0]);
                first = deckCntr.LoadLevel(lvlCardPresets[1]);
            }
            if (cardPanel != null && first != null)
            {
                //Setup the first card
                cardDisplay.LoadCard(first);

                //Setup the acceptButton
                GameObject buttonObj  = cardDisplay.transform.Find("AcceptButton").gameObject;
                Button button = buttonObj.GetComponent<Button>();
                button.onClick.AddListener(() => PlayCard(cardDisplay.card));
            }
            if (endTurnButton != null)
            {
                //Add Listener on the End Turn Button
                Button button = endTurnButton.GetComponent<Button>();
                button.onClick.AddListener(() => NewTurn());
            }
            if (exitApplicationButton != null)
            {
                //Add Listener on the exit button
                Button exitButton = exitApplicationButton.GetComponent<Button>();
                exitButton.onClick.AddListener(() => QuitApp() );
            }
        }


        // On the click of the EndTurn button, run this method
        public void NewTurn()
        {
            Card nextDraw = null;
            string failReason = null;


            //Return if card in play
            if (cardPanel.activeSelf) return;
            //Check for next level
            else if (stateCntr.CheckProgression())
            {
                if(cardDisplay?.card?.storyType is GreyType)
                {
                    //Display Popup
                    infoTitle.text = "Level " + level.ToString() + " Complete";
                    infoBody.text = "Congratulations! The company's profit margin is large enough to invest in upsizing!";
                    infoPanel.SetActive(true);
                    nextDraw = StepProgression();
                }
                else
                {
                    nextDraw = deckCntr.DrawGrey();

                    if (nextDraw == null)
                    {
                        //Null Draw Function will eventually go here
                        Debug.Log("Empty Draw");
                        nextDraw = EmptyDeckSolution();
                    }
                }
                
            }
            //Check for failure
            else if (stateCntr.FailCondition(ref failReason))
            {
                //Explain failure
                infoTitle.text = "Failure";
                infoBody.text = failReason;
                infoPanel.SetActive(true);
                nextDraw = StepFailure();
            }
            else
            {
                //Draw next card and prepare the panel for display
                nextDraw = deckCntr.DrawCard();
                // EmptyDeckSolution
                if (nextDraw == null)
                {
                    Debug.Log("Empty Draw");
                    nextDraw = EmptyDeckSolution();
                }
            }
          
            cardDisplay.DeactivateButtons();
            cardDisplay.LoadCard(nextDraw);

            cardPanel.SetActive(true);
        }

        // On the click of the 'accept' button on a card, run this function
        public void PlayCard(Card c)
        {
            if ( !c.HasActiveKey() ) return;

            // Pass Effect Bundle on to GameStateController
            SolutionBundle bundle = c.solutions[c.GetActiveKey()];
            List<Buff> DeckBuffs = stateCntr.AddBundleEffects(bundle.CardEffects);
            stateCntr.AddContextBundle(bundle.ContextBundle);

            // Pass remaining Deck Buffs onto DeckController
            deckCntr.AddProbabilities(DeckBuffs);

            // Apply stacked effects post new effects
            stateCntr.UpdateResources();
            stateCntr.UpdateContext();

            // Add nextCard if applicable
            NextCard( bundle );
            
            //Clean previous Card
            c.DeactivateKey();

            // TEMPORARY SOLUTION -> Will create an animate function to replace this line
            cardPanel.SetActive(false);
            
        }

        void NextCard(SolutionBundle bundle)
        {
            // NextCard
            deckCntr.LoadCard(bundle?.nextCard);
            // NextLVLCard
            if (level < 5) lvlCardPresets?[level + 1].LoadCard(bundle?.nextLvlCard);
        }

        Card StepProgression()
        {
            //Will need to UPDATE TO < 3 !!!!!!!
            if( level < 3 )
            {
                
                level += 1;

                //Update Resources 
                stateCntr.NextLevel();

                //Update Deck
                deckCntr.ClearDeck();

                //Update Visuals
                stateCntr.SetCompanyLevel(level - 1);

                //Load Misc and Level Cards
                deckCntr.LoadLevel(lvlCardPresets[0]);
                return deckCntr.LoadLevel(lvlCardPresets[level]);  
            }
            else
            {
                // Survived to the end game state! 
                // Lets draw your last card and see how you went ;) 
                return EndGame();
            }
        }

        Card EmptyDeckSolution()
        {
            int count = 0;
            string failReason = null;
            while( !stateCntr.CheckProgression() && count < 1000)
            {
                count += 1;
                stateCntr.UpdateCompanyResources();

                if( stateCntr.FailCondition(ref failReason) && count == 999)
                {
                    //Explain failure
                    infoTitle.text = "Failure";
                    if (failReason == null)
                    {
                        failReason = "Your Company plateaued in the market, failing to achieve your vision of an innovative, " +
                            "cutting edge technology company. Disappointed in your lack of achievement you retire." +
                            "Perhaps a more capable individual will take over the business.";
                    }
                    infoBody.text = failReason;
                    infoPanel.SetActive(true);
                    return StepFailure();
                }
            }

            //Display Popup
            infoTitle.text = "Level " + level.ToString() + " Complete";
            infoBody.text = "Congratulations! It took your company an additional " + count + 
                " turns for the company's profit margins to be large enough to invest in upsizing!";
            infoPanel.SetActive(true);

            // Step progression
            return StepProgression();
        }

        Card StepFailure()
        {
            //Reset Resources, Context and Clean Map
            stateCntr.ResetState();
            stateCntr.SetCompanyLevel(0);

            //Return to first level
            level = 1;
            deckCntr.ClearDeck();
            deckCntr.LoadLevel(lvlCardPresets[0]);
            return deckCntr.LoadLevel(lvlCardPresets[1]);
        }


        //Draw from endgame deck and disable card button listeners
        Card EndGame()
        {
            //Update Probabilities for an endgame state
            stateCntr.SetDominantState();
            deckCntr.SetEndDeckProbs(stateCntr.Preservation, stateCntr.Toxicity, stateCntr.Happiness, stateCntr.Stability,
                stateCntr.RandomPos, stateCntr.RandomNeg);

            deckCntr.LoadLevel(lvlCardPresets[4]);

            return deckCntr.DrawEnd();
        }


        // Update Company Name
        void Update()
        {
            cardDisplay.companyName = stateCntr.ReadCompanyName();
        }



        void CloseInfo()
        {
            infoPanel.SetActive(false);
        }

        void QuitApp()
        {
            Application.Quit();
        }

    }
}