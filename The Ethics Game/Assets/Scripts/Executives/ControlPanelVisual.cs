﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace EthicGame {
    public class ControlPanelVisual : MonoBehaviour
    {
        public GameObject CompanyNameObj;
        public GameObject CompanyLvlObj;
        public GameObject BankObj;
        public GameObject ManpwrObj;
        public GameObject SocialPosObj;
        public GameObject SocialNegObj;
        public GameObject EnvPosObj;
        public GameObject EnvNegObj;

        Text companyNameText;
        Text companyLvlText;
        Slider bankVis;
        Text bankText;
        Slider manpwrVis;
        Text manpwrCount;
        Slider socialPos;
        Slider socialNeg;
        Slider envPos;
        Slider envNeg;
        float[] barSliderVals = { 0f, 1.65f, 2.55f, 3.4f, 4.3f, 5.18f, 6f };

        
        // Use this for initialization
        void Awake()
        {
            //Flesh out private visual variables
            companyNameText = CompanyNameObj.transform.Find("Text")?.gameObject.GetComponentInChildren<Text>();
            if (companyNameText == null) Debug.Log("Could not find text child of CompanyName");

            companyLvlText = CompanyLvlObj.GetComponentInChildren<Text>();
            if (companyNameText == null) Debug.Log("Could not find text child of CompanyLevel");

            bankVis = BankObj.GetComponentInChildren<Slider>();
            bankText = BankObj.transform.Find("BankText")?.gameObject.GetComponentInChildren<Text>();
            if (bankVis == null) Debug.Log("Could not find the slider of bank");
            if (bankText == null) Debug.Log("Could not find text child of Bank");

            manpwrVis = ManpwrObj.GetComponentInChildren<Slider>();
            manpwrCount = ManpwrObj.transform.Find("ManpowerCount")?.gameObject.GetComponentInChildren<Text>();
            if (manpwrVis == null) Debug.Log("Could not find the slider of manpower");
            if (manpwrCount == null) Debug.Log("Could not find text child of manpower");

            socialPos = SocialPosObj.GetComponentInChildren<Slider>();
            socialNeg = SocialNegObj.GetComponentInChildren<Slider>();
            if (socialPos == null) Debug.Log("Could not find the pos slider of social");
            if (socialNeg == null) Debug.Log("Could not find the neg slider of social");

            envPos = EnvPosObj.GetComponentInChildren<Slider>();
            envNeg = EnvNegObj.GetComponentInChildren<Slider>();
            if (envPos == null) Debug.Log("Could not find the pos slider of environment");
            if (envNeg == null) Debug.Log("Could not find the neg slider of environment");

        }

        public void SetBank(long value, long capacity)
        {
            bankText.text = value.ToString();
            float bankVal = (float)value;
            float bankMax = (float)capacity;
            bankVis.value = bankVal / bankMax;
        }

        public void SetManpower(int value)
        {
            manpwrCount.text = value.ToString();

            //Cast to float and scale to 0-1
            float sliderVal = (float)value;
            sliderVal /= 100f;

            manpwrVis.value = sliderVal;
        }

        public void SetSocialPos(int value)
        {
            if (value > 95) socialPos.value = barSliderVals[6];
            else if(value > 80 ) socialPos.value = barSliderVals[5];
            else if(value > 60) socialPos.value = barSliderVals[4];
            else if(value > 40) socialPos.value = barSliderVals[3];
            else if(value > 20) socialPos.value = barSliderVals[2];
            else if(value > 0) socialPos.value = barSliderVals[1];
            else socialPos.value = barSliderVals[0];
        }

        public void SetSocialNeg(int value)
        {
            if (value < -95) socialNeg.value = barSliderVals[6];
            else if (value < -80) socialNeg.value = barSliderVals[5];
            else if (value < -60) socialNeg.value = barSliderVals[4];
            else if (value < -40) socialNeg.value = barSliderVals[3];
            else if (value < -20) socialNeg.value = barSliderVals[2];
            else if (value < 0) socialNeg.value = barSliderVals[1];
            else socialNeg.value = barSliderVals[0];
        }

        public void SetEnvPos(int value)
        {
            if (value > 95) envPos.value = barSliderVals[6];
            else if (value > 80) envPos.value = barSliderVals[5];
            else if (value > 60) envPos.value = barSliderVals[4];
            else if (value > 40) envPos.value = barSliderVals[3];
            else if (value > 20) envPos.value = barSliderVals[2];
            else if (value > 0) envPos.value = barSliderVals[1];
            else envPos.value = barSliderVals[0];
        }

        public void SetEnvNeg(int value)
        {
            if (value < -95) envNeg.value = barSliderVals[6];
            else if (value < -80) envNeg.value = barSliderVals[5];
            else if (value < -60) envNeg.value = barSliderVals[4];
            else if (value < -40) envNeg.value = barSliderVals[3];
            else if (value < -20) envNeg.value = barSliderVals[2];
            else if (value < 0) envNeg.value = barSliderVals[1];
            else envNeg.value = barSliderVals[0];
        }


        //Auxillary Functs
        public string GetCompanyName()
        {
            return companyNameText.text;
        }

        public void SetCompanyLvl(string lvl)
        {
            companyLvlText.text = lvl;
        }
    }
}