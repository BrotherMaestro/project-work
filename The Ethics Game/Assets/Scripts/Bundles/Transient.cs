﻿using UnityEngine;
using System.Collections;


namespace EthicGame
{
    public abstract class Transient : ScriptableObject
    {
        public int delay;
        public int duration;

        public Transient()
        {
            delay = 0;
            duration = -1;
        }
    }
}