﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



namespace EthicGame
{
    [CreateAssetMenu(menuName = "Bundles/ContextBundle")]
    public class ContextBundle : ScriptableObject
    {
        public List<ContextItem> ContextInfo;
        public List<TerrainBuff> terrainBuffs;
    }
}