﻿using UnityEngine;
using System.Collections;



namespace EthicGame
{
    public class AuraBuffLong<T> : Aura, IResourceBuff<T> where T : System.IComparable
    {
        public double baseProb;
        public T maxThreshold;
        public T minThreshold;
        IResource<long> resource;

        public bool Modify(ref T value)
        {
            if (delay > 0)
            {
                delay -= 1;
                return false;
            }
            if (duration == 0) return true;
            else if (duration > 0) duration -= 1;

            if (value.CompareTo(maxThreshold) >= 0)
            {
                ProbabilityBuffLong probBuff = CreateNew(baseProb, 0, 1, auraTarget);
                resource.AddBuff(probBuff);
            } 
            else if ( value.CompareTo(minThreshold) < 0)
            {
                ProbabilityBuffLong probBuff = CreateNew(-baseProb, 0, 1, auraTarget);
                resource.AddBuff(probBuff);
            }
            
            return false;
        }

        public void SetTarget(IResource<long> res)
        {
            resource = res;
        }


        ProbabilityBuffLong CreateNew(double prob, int delay, int duration, BuffType target)
        {
            ProbabilityBuffLong probBuff = CreateInstance("ProbabilityBuffLong") as ProbabilityBuffLong;

            probBuff.prob = prob;
            probBuff.delay = delay;
            probBuff.duration = duration;
            probBuff.buffTarget = target;

            return probBuff;
        }

    }
}