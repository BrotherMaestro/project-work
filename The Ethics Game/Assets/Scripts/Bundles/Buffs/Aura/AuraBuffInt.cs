﻿using UnityEngine;
using System.Collections;


namespace EthicGame
{
    public class AuraBuffInt<T> : Aura, IResourceBuff<T> where T : System.IComparable
    {
        public int baseVal;
        public T maxThreshold;
        public T minThreshold;
        IResource<int> resource;

        public bool Modify(ref T value)
        {
            if (delay > 0)
            {
                delay -= 1;
                return false;
            }
            if (duration == 0) return true;
            else if (duration > 0) duration -= 1;

            if (value.CompareTo(maxThreshold) >= 0)
            {
                SumBuffInt summation = CreateNew(baseVal, 0, 1, auraTarget);
                resource.AddBuff(summation);
            }
            else if (value.CompareTo(minThreshold) < 0)
            {
                SumBuffInt summation = CreateNew(-baseVal, 0, 1, auraTarget);
                resource.AddBuff(summation);
            }

            return false;
        }

        public void SetTarget(IResource<int> res)
        {
            resource = res;
        }

        SumBuffInt CreateNew(int sumVal, int delay, int duration, BuffType target)
        {
            SumBuffInt summation = CreateInstance("SumBuffInt") as SumBuffInt;
            summation.sum = sumVal;
            summation.delay = delay;
            summation.duration = duration;
            summation.buffTarget = target;

            return summation;
        }

    }
}