﻿using UnityEngine;
using System.Collections;


namespace EthicGame
{
    [CreateAssetMenu(menuName = "Buffs/Aura/Social->Manpower")]
    public class Social2Manpwr : AuraBuffInt<int>
    {

    }
}