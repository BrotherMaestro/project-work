﻿using UnityEngine;
using System.Collections;
using System;

namespace EthicGame
{
    [CreateAssetMenu(menuName = "Buffs/Probability/ScaledLong")]
    public class ScaledProbabilityBuffLong : ScaledProbability, IResourceBuff<long>
    {
        public ScaledProbabilityBuffLong(double probability, int delay, int duration) : base(delay, duration)
        {
            prob = probability;
        }


        public bool Modify(ref long value)
        {
            if (delay > 0)
            {
                value = 0;
                delay -= 1;
                return false;
            }
            if (duration == 0) return true;
            else if (duration > 0) duration -= 1;

            double tempVal = Convert.ToDouble(value);
            tempVal *= prob;
            value = Convert.ToInt64( Math.Round(tempVal) );
            return false;
        }
    }
}