﻿using UnityEngine;
using System.Collections;



namespace EthicGame
{
    public class ScaledProbability : Buff
    {
        public double prob;

        public ScaledProbability(int delay, int duration) : base(delay, duration)
        {

        }

    }
}