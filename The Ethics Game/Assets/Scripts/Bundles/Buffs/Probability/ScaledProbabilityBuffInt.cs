﻿using UnityEngine;
using System.Collections;
using System;

namespace EthicGame
{
    [CreateAssetMenu(menuName = "Buffs/Probability/ScaledInt")]
    public class ScaledProbabilityBuffInt : ScaledProbability, IResourceBuff<int>
    {
        public ScaledProbabilityBuffInt(double probability, int delay, int duration) : base(delay, duration)
        {
            prob = probability;
        }

        public bool Modify(ref int value)
        {
            if (delay > 0)
            {
                value = 0;
                delay -= 1;
                return false;
            }
            if (duration == 0) return true;
            else if (duration > 0) duration -= 1;

            double tempVal = Convert.ToDouble(value);
            tempVal *= prob;
            value = Convert.ToInt32( Math.Round(tempVal) );
            return false;
        }
    }
}