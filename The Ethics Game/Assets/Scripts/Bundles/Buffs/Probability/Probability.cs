﻿using UnityEngine;
using System.Collections;



namespace EthicGame
{
    public class Probability : Buff
    {
        public double prob;

        public Probability(int delay, int duration, BuffType target) : base(delay, duration, target)
        {

        }

    }
}