﻿using UnityEngine;
using System.Collections;
using System;

namespace EthicGame
{
    public class ProbabilityBuffDouble : Probability, IResourceBuff<double>
    {
        public ProbabilityBuffDouble(double probability, int delay, int duration, BuffType target) : base(delay, duration, target)
        {
            prob = probability;
        }
    
        public bool Modify(ref double value)
        {
            if (delay > 0)
            {
                value = 0;
                delay -= 1;
                return false;
            }
            if (duration == 0) return true;
            else if (duration > 0) duration -= 1;
 
            value *= prob;
            return false;
        }
    }
}