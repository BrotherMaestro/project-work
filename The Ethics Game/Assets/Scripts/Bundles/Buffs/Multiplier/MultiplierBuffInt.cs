﻿using UnityEngine;
using System.Collections;
using System;


namespace EthicGame
{
    public class MultiplierBuffInt : Multiplier, IResourceBuff<int>
    {

        public MultiplierBuffInt(double multiplier, int delay, int duration) : base(delay, duration)
        {
            mul = multiplier;
        }

        public bool Modify(ref int value)
        {
            if (delay > 0)
            {
                value = 0;
                delay -= 1;
                return false;
            }
            if (duration == 0) return true;
            else if (duration > 0) duration -= 1;

            double OrigVal = Convert.ToDouble(value);
            double MorphedVal = OrigVal * mul;
            MorphedVal -= OrigVal;
            value = Convert.ToInt32(Math.Round(MorphedVal));
            return false;
        }
    }
}