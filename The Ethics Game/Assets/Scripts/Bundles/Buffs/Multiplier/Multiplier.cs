﻿using UnityEngine;
using System.Collections;


namespace EthicGame
{
    public class Multiplier : Buff
    {
        public double mul;

        public Multiplier(int delay, int duration) : base(delay, duration)
        { }
    }
}