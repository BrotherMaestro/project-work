﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


namespace EthicGame
{
    [CreateAssetMenu(menuName = "Presets/TerrainPreset")]
    public class LvlTerrainPreset : ScriptableObject
    {
        public List<TerrainBuff> initialSetup;
        public List<TerrainBuff> DelayedConstruction;
    }
}