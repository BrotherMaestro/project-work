﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Tilemaps;
using System;

namespace EthicGame
{
    [CreateAssetMenu(menuName = "Bundles/TerrainBuff")]
    public class TerrainBuff : Buff, IEquatable<TerrainBuff>
    {
        public Tile tile;
        Tilemap tilemap;
        public bool obsolete;
        public List<Coord> coords;

        public Tilemap Tilemap { get => tilemap; set => tilemap = value; }


        public bool Equals(TerrainBuff other)
        {
            if (tile?.name == other.tile?.name 
                && Tilemap.Equals(other.Tilemap) &&
                CheckCoords(other) ) return true;
            else return false;
        }

        bool CheckCoords(TerrainBuff other)
        {
            bool result = false;
            foreach(Coord otherCoord in other.coords)
            {
                if (coords.Contains(otherCoord))
                {
                    result = true;
                }
                else
                {
                    return false;
                }
            }
            return result;
        }
    }
}