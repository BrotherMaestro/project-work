﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace EthicGame
{
    [CreateAssetMenu(menuName ="Bundles/TerrainBuffList")]
    public class TerrainBuffList : ScriptableObject
    {
        public List<TerrainBuff> terrainBuffs;
    }
}