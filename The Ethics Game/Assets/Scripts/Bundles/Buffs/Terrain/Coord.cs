﻿using UnityEngine;
using System.Collections;



namespace EthicGame
{
    [CreateAssetMenu(menuName = "Bundles/TerrainCoords")]
    public class Coord : ScriptableObject
    {
        public int x;
        public int y;
    }
}