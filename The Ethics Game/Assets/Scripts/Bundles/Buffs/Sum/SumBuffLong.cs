﻿using UnityEngine;
using System.Collections;


namespace EthicGame
{
    [CreateAssetMenu(menuName ="Buffs/Sum/Long")]
    public class SumBuffLong : Sum, IResourceBuff<long>
    {
        public long sum;

        SumBuffLong(long sumVal, int delay, int duration, BuffType target) : base(delay, duration, target)
        {
            sum = sumVal;
        }

        public bool Modify(ref long value)
        {
            if (delay > 0)
            {
                value = 0;
                delay -= 1;
                return false;
            }
            if (duration == 0) return true;
            else if (duration > 0) duration -= 1;

            value = sum;
            return false;
        }
    }

}