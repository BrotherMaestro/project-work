﻿using UnityEngine;
using System.Collections;



namespace EthicGame
{
    [CreateAssetMenu(menuName = "Buffs/Sum/Int")]
    public class SumBuffInt : Sum, IResourceBuff<int>
    {
        public int sum; 

        public SumBuffInt(int sumVal, int delay, int duration, BuffType target) : base(delay, duration, target)
        {
            sum = sumVal;
        }

        public bool Modify(ref int value)
        {
            if (delay > 0)
            {
                value = 0;
                delay -= 1;
                return false;
            }
            if (duration == 0) return true;
            else if (duration > 0) duration -= 1;

            value = sum;
            return false;
        }
    }
}