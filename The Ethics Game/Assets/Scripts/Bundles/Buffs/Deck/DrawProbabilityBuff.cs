﻿using UnityEngine;
using System.Collections;


namespace EthicGame
{
    [CreateAssetMenu(menuName = "Buffs/Probability/Deck")]
    public class DrawProbabilityBuff : Buff
    {
        public CardType deckType;
        public double shiftValue;

        public CardType GetDeckType()
        {
            return deckType;
        }

        public double GetShiftValue()
        {
            return shiftValue;
        }
    }
}