﻿using UnityEngine;
using System.Collections;


namespace EthicGame
{
    public interface IResourceBuff<T>
    {
        bool Modify(ref T value);
    }
}