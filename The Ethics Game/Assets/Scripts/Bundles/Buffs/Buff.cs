﻿using UnityEngine;
using System.Collections;

namespace EthicGame
{
    public abstract class Buff : Transient
    {
        public BuffType buffTarget;

        public Buff()
        {
            delay = 0;
            duration = -1;
        }

        public Buff(int delayT, int durationT)
        {
            delay = delayT;
            duration = durationT;
        }

        public Buff(int delayT, int durationT, BuffType target)
        {
            delay = delayT;
            duration = durationT;
            buffTarget = target;
        }

    }
}