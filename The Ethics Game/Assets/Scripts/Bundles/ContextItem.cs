﻿using UnityEngine;
using System.Collections;
using System;

namespace EthicGame
{
    [CreateAssetMenu(menuName = "Bundles/ContextItem")]
    public class ContextItem : Transient, IEquatable<ContextItem>
    {
        public ContextCategory contextCategory;
        public ContextClass contextClass;
        public bool obsolete;
        public string heading;
        public string content;

        public bool Equals(ContextItem other)
        {
            if (contextCategory == other.contextCategory &&
                contextClass == other.contextClass &&
                heading == other.heading &&
                content == other.content) return true;
            else return false;
        }
    }
}