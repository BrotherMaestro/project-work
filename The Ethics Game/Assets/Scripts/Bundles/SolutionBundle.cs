﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



namespace EthicGame
{
    [System.Serializable]
    public class SolutionBundle
    {
        public string solutionText;
        public Card nextCard;
        public Card nextLvlCard;
        public List<Buff> CardEffects;
        public ContextBundle ContextBundle;
    }
}